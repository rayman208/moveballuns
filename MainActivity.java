package com.example.moveballonsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        setContentView(new MyCanvas(getApplicationContext()));
    }

    public class MyCanvas extends View{

        int xMin, yMin, xMax, yMax;
        ArrayList<FunnySquare> funnySquares;

        public MyCanvas(Context context) {
            super(context);

            xMin = 0;
            yMin = 0;

            funnySquares = new ArrayList<>();

            for (int i = 0; i < 50; i++)
            {
                funnySquares.add(new FunnySquare());
            }
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            canvas.drawColor(Color.WHITE, PorterDuff.Mode.CLEAR);

            for (int i = 0; i < funnySquares.size(); i++)
            {
                funnySquares.get(i).Draw(canvas);
            }

            for (int i = 0; i < funnySquares.size(); i++)
            {
                funnySquares.get(i).Move(xMin,yMin,xMax,yMax);
            }


            invalidate();
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);

            xMax = w;
            yMax = h;
        }
    }
}
